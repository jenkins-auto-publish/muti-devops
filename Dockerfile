FROM registry.cn-hangzhou.aliyuncs.com/moxinqian/adoptopenjdk:1.0

ARG JAR_FILE=muti-devops.jar
ARG PORT=8095
ARG TIME_ZONE=Asia/Shanghai

ENV TZ=${TIME_ZONE}
ENV JVM_XMS="256m"
ENV JVM_XMX="256m"


COPY ${JAR_FILE} muti-devops.jar

HEALTHCHECK --interval=5s --timeout=3s CMD curl -fs http://localhost:8095/test || exit 1

EXPOSE ${PORT}
#指定prod
ENTRYPOINT java -Xms${JVM_XMS} -Xmx${JVM_XMX} -jar -Dspring.profiles.active=prod -Djava.security.egd=file:/dev/./urandom -server  muti-devops.jar