# muti-devops
muti-devops 是一个教会你使用 Docker、DockerCompose 构建 SpringBoot 应用的项目。后期会将 K8S 的部署方式集成。

## 项目描述
项目的核心应用是一个 SpringBoot 应用，为了演示组合构建，SpringBoot 包含两个接口，即：
+ 登录授权 `host:port/getToken`
这个接口的目的是为了使用Redis，将 token 信息缓存至 Redis

+ 获取数据 `host:port/getBooks?token=${token}`
这个接口的设计目的是为了用到 MySQL，在前一个接口获取授权后，调用这个接口获取数据。

因此前面总共用到了三个服务器即：SpringBoot+MySQL+Redis，在部署的时候考虑反向代理，会再添加一个 Nginx。


## 项目部署
如果你想用自己的 Nginx、MySQL、Redis，可以采用单体部署的方式，注意修改项目配置文件的 MySQL 和 Redis 地址即可。

### 单体部署步骤
仅为了学习 Docker ，可采用 Docker 单机部署的方式。步骤如下：  
+ 打包
```bash
mvn clean package
```
+ 构建镜像

```bash
docker build -t muti-devops .
```
+ 启动镜像

```bash
docker run -d --name muti-devops --restart=always -p8095:8095 muti-devops
```

### 组合部署步骤

组合部署用到四个服务，即 SpringBoot + MySQL + Redis + Nginx

+ 打包

```bash
mvn clean package
```

+ 启动服务

```bash
dcoker-compose up -d
```

## 部分文件说明
项目主要利用 maven 管理的 SpringBoot 应用，对其他文件做一个说明（自上而下）：

### conf.d
这个文件是 Nginx 容器的挂载目录，里面 muti.conf 配置了如何实现反向代理和负载均衡。

### init_sql
这个是 MySQL 启动的挂载目录，里面包含了一些初始化数据库的脚本。

### Dockerfile、docker-compose.yml
有关容器部署项目的两个文件。

### start.sh
启动服务脚本命令



