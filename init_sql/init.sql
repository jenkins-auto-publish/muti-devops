SET character_set_client = utf8;
SET character_set_results = utf8;
SET character_set_connection = utf8;
CREATE DATABASE  IF NOT EXISTS `book`;
USE `book`;
CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

REPLACE INTO `user` (id,username,password) VALUES(1, 'mxq', '123');
REPLACE INTO `book` (`author`, `name`, `desc`) VALUES ('周志明', '深入理解java虚拟机第三版', 'jvm高级特性与最佳实践（第3版）jvm虚拟机 java虚拟机');
REPLACE INTO `book` (`author`, `name`, `desc`) VALUES ('杨保华', 'Docker技术入门与实战（第3版）', '入门Docker的首本书，系统化掌握容器技术栈');
