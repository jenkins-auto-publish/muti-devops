package cn.czyfwpla.muti_devops.controller;

import cn.czyfwpla.muti_devops.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    /**
     * 授权
     */
    @RequestMapping(value = "/getToken", method = RequestMethod.POST)
    public Map auth(String username, String password) {
        return bookService.getToken(username, password);
    }

    /**
     * 获取数据
     *
     */
    @RequestMapping(value = "/getBooks", method = RequestMethod.GET)
    public Map getBooks(@RequestParam("token") String token) {
        if (!bookService.checkToken(token)) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", 1);
            map.put("msg", "token过期或错误");
            return map;
        } else {
            return bookService.getData();
        }
    }


}
