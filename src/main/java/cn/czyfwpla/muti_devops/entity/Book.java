package cn.czyfwpla.muti_devops.entity;

import lombok.Data;

@Data
public class Book {
    private Integer id;
    private String author;
    private String name;
    private String desc;
}
