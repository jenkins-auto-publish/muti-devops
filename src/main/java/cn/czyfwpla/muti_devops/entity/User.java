package cn.czyfwpla.muti_devops.entity;

import lombok.Data;

@Data
public class User {

    private Integer id;
    private String username;
    private String password;

}
