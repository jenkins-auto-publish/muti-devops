package cn.czyfwpla.muti_devops.mapper;

import cn.czyfwpla.muti_devops.entity.Book;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookMapper {

    List<Book> findAll();

}
