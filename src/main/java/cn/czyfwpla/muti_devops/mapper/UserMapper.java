package cn.czyfwpla.muti_devops.mapper;

import cn.czyfwpla.muti_devops.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    User findByUsername(String username);
}
