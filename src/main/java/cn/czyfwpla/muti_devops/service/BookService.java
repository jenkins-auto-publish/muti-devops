package cn.czyfwpla.muti_devops.service;

import cn.czyfwpla.muti_devops.entity.User;
import org.springframework.stereotype.Service;

import java.util.Map;


public interface BookService {


    Map getToken(String username, String password);

    Map getData();

    boolean checkToken(String token);

}
