package cn.czyfwpla.muti_devops.service.Impl;

import cn.czyfwpla.muti_devops.entity.Book;
import cn.czyfwpla.muti_devops.entity.User;
import cn.czyfwpla.muti_devops.mapper.BookMapper;
import cn.czyfwpla.muti_devops.mapper.UserMapper;
import cn.czyfwpla.muti_devops.service.BookService;
import cn.czyfwpla.muti_devops.tools.TokenTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private BookMapper bookMapper;

    @Override
    public Map getToken(String username, String password) {

        Map<String, Object> map = new HashMap<>();

        User byUsername = userMapper.findByUsername(username);
        if (byUsername == null) {
            map.put("code", 1);
            map.put("msg", "用户名或密码错误");
        } else {
            if (byUsername.getPassword().equals(password)) {
                String token = TokenTools.getInstance().makeToken();
                map.put("code", 0);
                map.put("msg", "成功");
                map.put("token", token);
                //保存token，并将过期时间设置为10分钟
                redisTemplate.opsForValue().set(token, username, 10, TimeUnit.MINUTES);
                Long expire = redisTemplate.opsForValue().getOperations().getExpire(token);
                Date date = new Date();
                Date expireDate = new Date(new Date().getTime() + expire * 1000);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String expireTime = dateFormat.format(expireDate);
                map.put("expire", expireTime);
            } else {
                map.put("code", 1);
                map.put("msg", "用户名或密码错误");
            }
        }
        return map;
    }

    @Override
    public Map getData() {

        Map<String, Object> map = new HashMap<>();
        List<Book> all = bookMapper.findAll();
        map.put("code", 0);
        map.put("data", all);
        return map;
    }

    @Override
    public boolean checkToken(String token) {
        return redisTemplate.opsForValue().getOperations().getExpire(token) != -2;

    }
}
